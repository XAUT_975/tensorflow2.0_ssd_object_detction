import os
import sys
import tensorflow as tf

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# def export_tflite_from_savedmodel(savedmodel, tflite_model):
#     tf.lite.TFLiteConverter.from_saved_model()

if __name__ == '__main__':
    # https://blog.csdn.net/zz531987464/article/details/105949005
    # saved_model_to_tflite_converter = tf.lite.TFLiteConverter.from_saved_model('/home/wei/Ubuntu/Project/TF/ssd_mobilenet_v2_320x320_coco17_tpu-8/saved_model')
    # saved_model_to_tflite_converter.experimental_new_converter = True
    # saved_model_tflite = saved_model_to_tflite_converter.convert()
    # print(saved_model_tflite)

    PATH = r'/home/wei/Ubuntu/Project/TF/ssd_mobilenet_v2_320x320_coco17_tpu-8/saved_model'
    converter = tf.lite.TFLiteConverter.from_saved_model(PATH,signature_keys=['serving_default'])
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    converter.experimental_new_converter = True
    converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS, tf.lite.OpsSet.SELECT_TF_OPS]
    tflite_model = converter.convert()
    open("model.tflite", "wb").write(tflite_model)