# 数据集加载

## 训练数据集格式
* image
* label

![111](./doc/1.png)

![111](./doc/2.png)

![111](./doc/3.png)

JSON格式

```
{
    "path": "../image/IMG_20200616_170949.jpg",
    "outputs": {
        "object": [
            {
                "name": "figure",
                "bndbox": {
                    "xmin": 1856,
                    "ymin": 1994,
                    "xmax": 2089,
                    "ymax": 2180
                }
            },
            {
                "name": "hand_small",
                "bndbox": {
                    "xmin": 1835,
                    "ymin": 1997,
                    "xmax": 3008,
                    "ymax": 3655
                }
            },
            {
                "name": "hand_large",
                "bndbox": {
                    "xmin": 1840,
                    "ymin": 1992,
                    "xmax": 3696,
                    "ymax": 4536
                }
            }
        ]
    },
    "time_labeled": 1264851152887,
    "labeled": true,
    "size": {
        "width": 3840,
        "height": 5120,
        "depth": 3
    }
}
```

---
# 模型文件下载
**Tensorflow/models Repo 下载**

![111](./doc/4.png)

---

# 模型训练
注意修改模型路径，训练集路径
## Steps
1. Prepare dataset
2. Load pretraind model
3. Fine-tuning/Training
4. Save Model 

## Parameters
- num_epoch
- batch_size: 一般32,64,128
- learning_rate

训练脚本
```
python train.py
```
设置使用GPU
```
os.environ['CUDA_VISIBLE_DEVICES'] = '0'  (GPU id: 0)
```
训练过程：Loss总体减小，存在波动

![](./doc/6.png)

训练完成，模型保存

![](./doc/7.png)

---

# TFLite 模型转换
注意修改模型路径
```
python ./tflite_tools/tflite_utils.py
```
